import java.util.Scanner;
public class GameLauncher{
	public static void main(String[] args){
		Scanner sc= new Scanner(System.in);
		System.out.println("Welcome to the game launcher!\nFrom here, you can choose between two games:\n");
		System.out.print("\nHangman: type 1 to begin.\nWordle: type 2 to begin.\n=>");
		int playerGameChoice= sc.nextInt();
		sc.nextLine();
		if (playerGameChoice==1){
			TheHangman_Complex Hangman= new TheHangman_Complex();
			
			System.out.println("Let's play Hangman! One player will choose a word. The second player will have to guess that word.");
			System.out.print("\nPlayer 2, please look away from the screen.Player 1, please configure the settings:\n\n");
			
			System.out.print("Word to be guessed:");
			String chosenWord = sc.nextLine();
			
			System.out.print("\nNumber of Guesses Player 2 can make:");
			int NumberOfChances= sc.nextInt();
			sc.nextLine();
			boolean[] ListOfLettersGuessed = new boolean[chosenWord.length()];
			System.out.print(String.format("\033[2J"));
			System.out.println("Player 2, you can start guessing by inputing one letter per guess. You have "+ NumberOfChances +" guesses left.");
			Hangman.runGame(chosenWord,ListOfLettersGuessed, NumberOfChances);
		} 
		if (playerGameChoice==2){
			Wordle_complex Wordle = new Wordle_complex();
			
			
			String WordToBeGuessed = Wordle.generateWord().toUpperCase();
			int TargetLength = WordToBeGuessed.length();
			
			System.out.println("Welcome to Wordle! You have " + (TargetLength + 1)
                + " chances to guess the " + TargetLength + "-letter word. Good luck!");
			Wordle.runGame(WordToBeGuessed);
		}
		else{
			
		}
	}
}	