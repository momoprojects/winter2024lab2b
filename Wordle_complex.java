import java.util.Scanner;
import java.util.Random;
public class Wordle_complex {
    // this version is only a little more complex.
    // It uses a bigger list of words that can have repeating letters and is not
    // explicitly limited to 5 letters.
    // The game dynamically changes with the length of the word
    public static void main(String[] args) {

        // invokes target word
        String WordToBeGuessed = generateWord().toUpperCase();
        int TargetLength = WordToBeGuessed.length();

        // by the rules of original wordle, player has 6 chances for a five letter word
        // (so 5+1 chances).
        // this is why I used target word length + 1
        System.out.println("Welcome to Modified Wordle! You have " + (TargetLength + 1)
                + " chances to guess the " + TargetLength + "-letter word. Good luck!");

        // runs main function
        runGame(WordToBeGuessed);
    }

    // returns a random word from the list
    public static String generateWord() {
        Random random = new Random();
        // list of all possible target words
        String[] ListOfWords = {
                "apple", "banana", "orange", "grape", "kiwi",
                "elephant", "giraffe", "lion", "tiger", "zebra",
                "computer", "keyboard", "mouse", "monitor", "printer",
                "ocean", "mountain", "forest", "desert", "river",
                "book", "pen", "pencil", "notebook", "marker",
                "sunflower", "rose", "tulip", "daisy", "lily",
                "football", "basketball", "soccer", "tennis", "volleyball",
                "music", "guitar", "piano", "drums", "violin",
                "rainbow", "cloud", "sky", "sun", "moon",
                "pizza", "burger", "spaghetti", "sushi", "cosmos",
                "movie", "popcorn", "actor", "actress", "director",
                "fireplace", "candle", "cozy", "blanket", "pillow",
                "rocket", "planet", "star", "galaxy", "astronaut",
                "train", "car", "bicycle", "bus", "plane",
                "jazz", "rock", "pop", "classical", "hip-hop",
                "island", "beach", "wave", "surf", "camera",
                "photo", "snapshot", "filter", "selfie",
                "adventure", "exploration", "discovery", "journey", "quest"
        };
        // chooses a random word from the list
        return ListOfWords[random.nextInt(ListOfWords.length)];
    }

    // returns true if letter is somewhere in the target word
    public static boolean letterInWord(String word, char letter) {
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) == letter)
                return true;
        }
        return false;
    }

    // returns true if letter is in exact position in target word
    public static boolean letterInSlot(String word, char letter, int position) {
        return word.charAt(position) == letter;
    }

    // returns an array of colors to use for each letter:
    // "green" for correct letter in correct position,
    // "yellow" for correct letter in wrong position,
    // "white" for wrong letter
    public static String[] guessWord(String answer, String guess) {
        String[] colors = new String[answer.length()];
        for (int i = 0; i < answer.length(); i++) {
            if (letterInSlot(answer, guess.charAt(i), i))
                colors[i] = "green";
            else if (letterInWord(answer, guess.charAt(i)))
                colors[i] = "yellow";
            else
                colors[i] = "white";
        }
        return colors;
    }

    // uses array list of guessWord() to print the progress of the player
    public static void presentResults(String word, String[] colors) {
        String progress = "\nYour progress: ";
        final String ANSI_RESET = "\u001B[0m";
        final String ANSI_GREEN = "\u001B[32m";
        final String ANSI_YELLOW = "\u001B[33m";
        final String ANSI_WHITE = "\u001B[37m";
        for (int i = 0; i < word.length(); i++) {
            if (colors[i].equals("green")) {
                progress += ANSI_GREEN+ word.charAt(i) + ANSI_RESET;
            } else if (colors[i].equals("yellow")) {
                progress += ANSI_YELLOW + word.charAt(i) + ANSI_RESET;
            } else {
                progress += ANSI_WHITE + word.charAt(i) + ANSI_RESET;
            }
        }
        System.out.println(progress);
    }
    // reads player guess and makes sure there are no issues.
    public static String readGuess(int answerLength) {
        System.out.print("\nPlease enter your guess: ");
        Scanner sc = new Scanner(System.in);
        String PlayerGuess = sc.nextLine();
        if (PlayerGuess.length() != answerLength) {
            System.out.println("Your guess must be " + answerLength + " letters long!");

            // i wanted to use recursion, but AI suggested to put "return" before
            // the recursion because otherwise it would lead to a stack overflow
            return readGuess(answerLength);
        }
        return PlayerGuess.toUpperCase();
    }

    // main loop of the game
    public static void runGame(String target) {
        int guessChances = target.length() + 1;
        while (guessChances > 0) {
            String PlayerGuess = readGuess(target.length());

            if (target.equals(PlayerGuess)) {
                if (guessChances == 1)
                    System.out.println("You won! You had " + guessChances + " guess left");
                else
                    System.out.println("You won! You had " + guessChances + " guesses left");
                return;
            } else
                guessChances--;
            presentResults(PlayerGuess, guessWord(target, PlayerGuess));

            if (guessChances == 1)
                System.out.println("You have " + guessChances + " guess left");
            else
                System.out.println("You have " + guessChances + " guesses left");
        }
        System.out.println("You lost! The word was " + target);
    }
}