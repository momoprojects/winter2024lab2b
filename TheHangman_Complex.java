import java.util.Scanner;
public class TheHangman_Complex{
	
	//functions
	public static boolean isLetterinWord(String word, char c, boolean[] ListOfLettersGuessed){
		//returns postition of  c in word. if c is not  in word, return -1.
		boolean LetterIsInWord=false;
		for (int i=0;i<word.length();i++){
			if (toUpperCase(word.charAt(i))==toUpperCase(c)){
				ListOfLettersGuessed[i]=true;
				LetterIsInWord=true;
			}
		}
		return LetterIsInWord;
	}
	// returns char as uppercase
	public static char toUpperCase(char c){
		return (Character.toString(c).toUpperCase()).charAt(0);
	}
	//prints the letters that have been guessed correctly. If letter hasnt been guessed, substitute with an underline.
	public static void printWork(String word,boolean[] ListOfLettersGuessed){
		String output="";
		for (int i=0;i<word.length();i++){
			if (ListOfLettersGuessed[i]==true) output=output+" "+word.charAt(i);
			else output=output+" _";
		}

		System.out.println("Your result is ["+output+"]");
		
		
	}
	public static void runGame(String word, boolean[] ListOfLettersGuessed, int guessChances){

		Scanner scanner = new Scanner(System.in);

		while(guessChances>=0){ 
		
			//if all letters gussed, break loop and declare winner
			int hasWon=0;
			for (int i=0;i<word.length();i++){
				if (!ListOfLettersGuessed[i]){
					hasWon++;
				}
			}
			if (hasWon==0){
				System.out.println("Player 2 has won! the word was "+word+". They had "+guessChances+" chances left.");
				break;
			}
			// if guesses have run out, break loop and declare winner
			if (guessChances==0) {
				
				System.out.println("Player 1 has won! the word was "+word+".");
				break;
				}
			System.out.print("\nYour next guess:");
			
			//player guess 
			char Player_guess= toUpperCase(scanner.next().charAt(0));
			
			if (!isLetterinWord(word, Player_guess, ListOfLettersGuessed)  ){	
				//player  guessed wrongly!
				guessChances--;
				System.out.print("\nYou guessed wrongly.");
				printWork(word, ListOfLettersGuessed);
				System.out.println("You have "+guessChances+" chances left.");
				
			}
			else{
				//player guessed right!
				System.out.print("\nYou guessed correctly.");

				printWork(word,ListOfLettersGuessed);
			} 
		}
	}	
}	